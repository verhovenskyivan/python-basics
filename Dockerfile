FROM python:alpine
RUN pip install --upgrade pip
WORKDIR /app

COPY . .

CMD [ "python", "*.py" ]
